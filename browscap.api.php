<?php

/**
 * @file
 * Hooks for the browscap module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the browser properties value.
 *
 * @param array $properties
 *   Browser properties.
 * @param string $user_agent
 *   User agent.
 */
function hook_browscap_properties_alter(&$properties, $user_agent) {
  $properties['comment'] = t('My comment...');
}

/**
 * @} End of "addtogroup hooks".
 */
