<?php

namespace Drupal\browscap;

use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use GuzzleHttp\Exception\RequestException;

/**
 * Browscap endpoint.
 *
 * Defines methods for communicating with Browscap project website.
 */
class BrowscapEndpoint {

  use StringTranslationTrait;

  /**
   * Gets version of latest Browscap data.
   *
   * @return int|string
   *   Either an error code (BROWSCAP_IMPORT_VERSION_ERROR) or the latest
   *   Browscap data version.
   */
  public function getVersion() {
    $config = \Drupal::config('browscap.settings');
    // Retrieve the current browscap data version number using HTTP.
    $client = \Drupal::httpClient();

    $current_version = '';
    try {
      $browscapVersionURL = $config->get('version_url');
      $response = $client->get($browscapVersionURL);
      // Expected result.
      $current_version = (string) $response->getBody();
    }
    catch (RequestException $e) {
      \Drupal::logger('browscap')->error($e->getMessage());
    }

    // Log an error if the browscap version number could not be retrieved.
    if (isset($current_version->error)) {
      // Log a message with the watchdog.
      \Drupal::logger('browscap')
        ->error("Couldn't check version: %error", ['%error' => $current_version->error]);
      return BrowscapImporter::BROWSCAP_IMPORT_VERSION_ERROR;
    }

    // Sanitize the returned version number.
    $current_version = Html::escape(trim($current_version));

    return $current_version;
  }

  /**
   * Gets latest Browscap data.
   *
   * @param bool $cron
   *   Whether this method is being invoked by cron.
   *
   * @return int|string
   *   Either an error code (BROWSCAP_IMPORT_DATA_ERROR) or the Browscap data.
   */
  public function getBrowscapData($cron = TRUE) {
    $config = \Drupal::config('browscap.settings');
    $client = \Drupal::httpClient();

    // Set options for downloading data with or without compression.
    if (function_exists('gzdecode')) {
      $options = [
        'headers' => ['Accept-Encoding' => 'gzip'],
      ];
    }
    else {
      // The download takes over ten times longer without gzip, and may exceed
      // the default timeout of 30 seconds, so we increase the timeout.
      $options = ['timeout' => 600];
    }

    $response = $browscap_data = '';

    // Retrieve the browscap data using HTTP.
    try {
      $browscapDataURL = $config->get('data_url');
      $response = $client->get($browscapDataURL, $options);

      // getBody will decompress gzip if need be.
      $browscap_data = (string) $response->getBody();
    }
    catch (RequestException $e) {
      Error::logException(\Drupal::logger('browscap'), $e);
    }

    // Log an error if the browscap data could not be retrieved.
    if (isset($response->error)) {
      // Log a message with the watchdog.
      \Drupal::logger('browscap')
        ->error("Couldn't retrieve updated browscap: %error", ['%error' => $response->error]);

      // Display a message to the user if the update process was triggered
      // manually.
      if ($cron == FALSE && !empty($response->error)) {
        \Drupal::messenger()->addError($this->t("Couldn't retrieve updated browscap: %error", ['%error' => $response->error]));
      }

      return BrowscapImporter::BROWSCAP_IMPORT_DATA_ERROR;
    }

    return $browscap_data;
  }

}
