<?php

namespace Drupal\Tests\browscap\Functional;

use Drupal\browscap\BrowscapEndpoint;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests browscap service.
 *
 * @group browscap
 */
class BrowscapImportTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['browscap'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests importing then querying Browscap data.
   */
  public function testImport() {
    // Force import all data.
    $endpoint = new BrowscapEndpoint();
    \Drupal::service('browscap.importer')->import($endpoint, FALSE);

    // Make browscap service call to check that data was loaded.
    $user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36';
    $properties = \Drupal::service('browscap')->getBrowser($user_agent);
    $this->assertEquals($properties['browser'], 'Chrome', 'Uncached browser is correct.');
    $this->assertEquals($properties['version'], '101.0', 'Uncached browser version is correct.');
    $this->assertEquals($properties['platform'], 'Linux', 'Uncached platform is correct.');

    // Now that the data is cached, try again.
    $properties = \Drupal::service('browscap')->getBrowser($user_agent);
    $this->assertEquals($properties['browser'], 'Chrome', 'Cached browser is correct.');
    $this->assertEquals($properties['version'], '101.0', 'Cached browser is correct.');
    $this->assertEquals($properties['platform'], 'Linux', 'Cached browser is correct.');
  }

}
