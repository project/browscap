# Browscap

Browscap provides an improved version of PHP's get_browser() function.

The get_browser() function can be used to tell what a visitor's browser is
capable of. Unfortunately, the version provided by PHP has a number of
limitations, namely:

* It can be difficult or impossible to configure for shared hosting
  environments.
* The data used to identify browsers and determine their capabilities requires
  consistent maintenance to keep up-to-date.

Browscap automates maintenance by storing browser data in a database and
automatically retrieving the latest data on a configurable schedule.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/browscap).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/browscap).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is needed.


## Maintainers

- Devin Carlson - [devin-carlson](https://www.drupal.org/u/devin-carlson)
- Greg Knaddison - [greggles](https://www.drupal.org/u/greggles)
- Matt Lucasiewicz - [lahoosascoots](https://www.drupal.org/u/lahoosascoots)
- Mike Ryan - [mikeryan](https://www.drupal.org/u/mikeryan)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
